package jp.alhinc.inoue_jonin.caluculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalculateSales {

	public static void main(String[] args){
		HashMap<String, String> store = new HashMap<>();             //マップを準備する
		HashMap<String, Long> storeresults = new HashMap<String, Long>();
		HashMap<String, String> listbox = new HashMap<>();           //売り上げファイルのマップ

		BufferedReader br = null;                         // brは支店番号と支店名のインプット情報を格納
		BufferedReader br2 = null;
		try{
			File file = new File(args[0], "branch.lst");
			FileReader fr = new FileReader( file );
			br = new BufferedReader( fr );





			//「branch.lst」から支店コードと支店名を読み込む
			// 支店コードと支店名をリンクさせながら、マップにputする。
			String line;
			Pattern p = Pattern.compile("^\\d{3}$");  // matchesメソッドが簡単 文字列の部分一致 2018.09.11

			while(( line = br.readLine()) != null){
				String[] storeinfo = line.split(",") ;   //引数「2」は2個目以降は切る 2018.09.11 => カンマがでたんだけど、、、
				//System.out.println(storeinfo[1]);
				Matcher match = p.matcher(storeinfo[0]);



				//支店コードが数字3つで成り立たなかったら不正表示
				//支店名にカンマがあれば不正表示

				if(match.find()){
					if(storeinfo[1].matches(".*,.*")){
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}else{
							store.put(storeinfo[0], storeinfo[1]);
					}

				}else{
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}





				//支店コードをマップにputする。
				//支店コードをリスト化する。

				storeresults.put(storeinfo[0],0L);
			}
			List<String> storelist = new ArrayList<>(store.keySet());


			try{
				long jojo = 0 ;
				long listvalues = 0; //後ほど売り上げファイルを数えるのに使う
				String codenum =null;



				//フォルダ内のファイルをリスト化する。

				File folder = new File(args[0]);
				String[] fileList = folder.list();





				//ファイル名8文字かつ拡張子rcdのファイルのみ読み込む

				for(String s : fileList){
					String[] filelineinfo = s.split("\\.");
					int num1 = filelineinfo[0].length();                 //num1 => 拡張子を除くファイルの文字数  num2 => 拡張子
					boolean num2 = filelineinfo[1].equals("rcd");




					//ファイルが連番か確認する。

					if((num1 == 8) && (num2) ){
						File folder2 = new File(args[0], s);
						FileReader fr2 = new FileReader( folder2);
						br2 = new BufferedReader(fr2);



						//ファイル名が数字のみか確認する

						if(filelineinfo[0].matches("\\D")){
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
						listvalues ++ ;



						//読み込んだファイルの1行目、2行目を読み込む。
						//2行目は整数にする。

						String filedata1;
						long filedata2;
						String filedata2a;
						String nodata;

						filedata1 = br2.readLine();      // br2を1行読み込む
						filedata2a = br2.readLine();      // br2をさらに1行読み込む
						if((nodata = br2.readLine()) != null){
							System.out.println(s + "のフォーマットが不正です");
							return;
						}
						filedata2 = Long.parseLong(filedata2a);
						listbox.put(filelineinfo[0],filelineinfo[1]);     //ファイル数を数える用

						codenum = filedata1;




						//キー配列（支店コード）を検索し、同じコードの場合、値を追加
						//支店コードとマッチする売り上げ金額を参照する。
						//売り上げ金額を追加する。

						for(String jo : storelist){
							if(jo.equals(filedata1)){
								jojo = storeresults.get(filedata1);
								jojo += filedata2;
								break;
							}
						}


						//支店コードと売り上げ金額をマップに入れる。

						String jojojo2 = String.valueOf(jojo);
						long jojojojo = jojojo2.length();
						if(jojojojo >= 10){
							System.out.println("合計金額が10桁を超えました");
							return;
						}else{
						storeresults.put(filedata1,jojo);
						}

					}else{
						continue;   //この項目の繰り返しを除く
					}


					//不正コードを検索する

					if(!store.containsKey(codenum)){
						System.out.println(s + "の支店コードが不正です");
						return;
					}
				}

				//売り上げファイルがない場合のエラー表示

				if(listvalues == 0){
					System.out.println("売り上げファイルがありません");
					return;
				}


				List<String> resultlist = new ArrayList<>(listbox.keySet());

				//System.out.println(resultlist.size());


				long k ;
					ArrayList<Long> listbox2 = new ArrayList<Long>();

				for(int i = 0; i < listvalues; i++ ){
					String a = resultlist.get(i);
					k = Long.parseLong(a);
					//System.out.println(k);
					listbox2.add(k);
				}
				//System.out.println(listvalues);



				//売り上げファイルを昇順にする

				Collections.sort(listbox2);


				//昇順にしたファイルを1から順に照らし合わせ
				//抜け番が発生したときにエラー表示する

				for(int i = 0; i < listvalues; i++ ){
					long ss = listbox2.get(i);
					if(ss != i+1){
						System.out.println("売り上げファイル名が連番になっていません");
						return;
					}else{
					//System.out.println(ss);

					}
				}

			} catch( IOException e){
				System.out.println("予期せぬエラーが発生しました");
			} finally {
				//System.out.println("finally処理しています");

				if(br2 != null){
					try{
						br2.close();
					}catch( IOException e){
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}
		}catch( IOException e){
			System.out.println( "支店定義ファイルがありません" );

		} finally {
			//System.out.println("finally処理しています");
			if(br != null){
				try {
					br.close();
				}catch( IOException e){
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}







		/*
		 以下からファイルに出力するプログラム
		 */

		try{

			//「branch.out」へ出力する準備。

			File file2 = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter( file2 );
			BufferedWriter bw = new BufferedWriter( fw );


			//支店コードをリスト化する。
			//支店コードのリストがなくなるまで繰り返す。

			List<String> resultlist = new ArrayList<>(storeresults.keySet());
			while(resultlist != null){


			//支店コードとリンクする「支店名」と「合計売り上げ金額」を参照する
			//「支店コード,支店名,集計金額」で出力する。

				String b;
				String c;
				long d;
					for(String jojojo :resultlist){
						if(jojojo.equals(jojojo)){
							b = store.get(jojojo);
							d = storeresults.get(jojojo);
							c = String.valueOf(d);
							bw.write(jojojo +","+ b +","+ c+"\r\n");
						}
					}
					bw.close();
				}
			}catch( IOException e){
		}
	}
}